﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace оценки
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int N = 22, res5 = 0, res4 = 0, res3 = 0, res2 = 0;
            Random rnd = new Random();
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(2, 5+1);
                Console.Write($"{mas[i]} ");
            }
            #endregion
            for (int i = 0; i < N; i++)
            {
                switch (mas[i])
                {
                    case 5: res5++; break;
                    case 4: res4++; break;
                    case 3: res3++; break;
                    default: res2++; break;
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Количество пятерок: {res5}");
            Console.WriteLine($"Количество четверок: {res4}");
            Console.WriteLine($"Количество троек: {res3}");
            Console.WriteLine($"Количество двоек: {res2}");
            Console.ReadKey();
        }
    }
}
