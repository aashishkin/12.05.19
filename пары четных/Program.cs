﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace пары_четных
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int N =10, res = 0;
            Random rnd = new Random();
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(0, 10);
                Console.Write($"{mas[i]} ");
            }
            #endregion
            for (int i = 0; i < N-1; i++)
            {
                if (mas[i] % 2 == 0 && mas[i+1] % 2 == 0)
                {
                    res++;
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Количество пар: {res}");
            Console.ReadKey();
        }
    }
}
