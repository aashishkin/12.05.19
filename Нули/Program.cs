﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Нули
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int res = 0, maxchain = 0, memindex = 0, maxmemindex = 0;
            Random rnd = new Random();
            int N = rnd.Next(1, 100);
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(0, 2);
                Console.Write(mas[i]);
            }
            #endregion
            for (int i = 0; i < N; i++)
            {
                if (mas[i] == 0)
                {
                    res++;
                    if (memindex == 0)
                    {
                        memindex = i;
                    }
                }
                else
                {
                    if (maxchain < res)
                    {
                        maxchain = res;
                        maxmemindex = memindex;
                    }
                    res = 0;
                    memindex = 0;
                }
            }
            
            Console.WriteLine();
            for (int i = 0; i < N; i++)
            {
                if (i >= maxmemindex & i < maxmemindex+maxchain)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{mas[i]}");
                    Console.ResetColor();
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.Write($"{mas[i]}");
                    Console.ResetColor();
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Длина максимальной цепи: {maxchain}");
            Console.ReadKey();
        }
    }
}
