﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace подмассив_массива
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int N = 10, j= 0, a = 0, b = 0;
            Random rnd = new Random();
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(0, 10);
                Console.Write($"{mas[i]} ");
            }
            #endregion
            Console.WriteLine("Количество подмассивов: ");
            j = int.Parse(Console.ReadLine());
            Console.WriteLine();
            for (int i = 0; i < j; i++)
            {
                Console.WriteLine($"Нижняя граница подмассива {i + 1}: ");
                a = int.Parse(Console.ReadLine());
                Console.WriteLine($"Верхняя граница подмассива {i + 1}: ");
                b = int.Parse(Console.ReadLine());
                if (a<b && a >= 1 && b < N)
                {
                    for (int z = a - 1; z <= b - 1; z++)
                    {
                        Console.Write($"{mas[z]} ");
                    }
                    Console.WriteLine();
                }
                else if (a > 1 && b < N)
                {
                    for (int z = a - 1; z >= b - 1; z--)
                    {
                        Console.Write($"{mas[z]} ");
                    }
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("Неверные параметры");
                    Console.WriteLine();
                }
                
            }
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}
