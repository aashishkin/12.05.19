﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace максимальная_пятерка
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int N = 20, res = 0, memindex = 0;
            Random rnd = new Random();
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(0, 10);
                Console.Write($"{mas[i]} ");
            }
            #endregion
            for (int i = 0; i < N - 4; i++)
            {
                if (res < (mas[i] + mas[i+1] + mas[i+2] + mas[i+3] + mas[i+4]))
                {
                    res = mas[i] + mas[i + 1] + mas[i + 2] + mas[i + 3] + mas[i + 4];
                    memindex = i;
                }
            }
            for (int i = 0; i < N; i++)
            {
                if (i >= memindex && i <= memindex+4)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write($"{mas[i]} ");
                    Console.ResetColor();
                }
                else
                {
                    Console.Write($"{mas[i]} ");
                }

            }
            Console.WriteLine();
            Console.WriteLine($"Максимальная сумма: {res}");
            Console.ReadKey();
        }
    }
}
