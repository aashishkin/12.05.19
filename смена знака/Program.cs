﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace смена_знака
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Объявление переменных
            int N = 10, res = 0, znak = 0;
            Random rnd = new Random();
            int[] mas = new int[N];
            #endregion
            #region Заполнение массива рандомно ненулевыми
            for (int i = 0; i < N; i++)
            {
                mas[i] = rnd.Next(1, 100);
                znak = rnd.Next(0, 2);
                if (znak == 1)
                {
                    mas[i] *= -1;
                }
                Console.Write($"{mas[i]} ");
            }
            #endregion
            for (int i = 0; i < N - 1; i++)
            {
                if ((mas[i] > 0 && mas[i + 1] < 0) || (mas[i] < 0 && mas[i + 1] > 0))
                {
                    res++;
                }
                
            }
            Console.WriteLine();
            Console.WriteLine($"Знак сменился {res} раз");
            Console.ReadKey();
        }
    }
}
